﻿using System.Web;
using System.Web.Mvc;

namespace S00123499MovieActorApp
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}