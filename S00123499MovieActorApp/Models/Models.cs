﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace S00123499MovieActorApp.Models
{

    public class ActorMovoeDBInitialiser : DropCreateDatabaseAlways<ActorMovieDB>
    {
        protected override void Seed(ActorMovieDB context)
        {

            var movies = new List<Movie>() {   
                            new Movie(){MovieTitle="The Godfather",Rating=5,ImageUrl="~/Content/Images/The Godfather.jpg",AgeLimit="18"},
                            new Movie(){MovieTitle="Ben Hur",Rating=3,ImageUrl="~/Content/Images/Ben hur.jpg",AgeLimit="PG"},
                            new Movie(){MovieTitle="The Godfather II",Rating=5,ImageUrl="~/Content/Images/The_Godfather_Part_2-Al-Pacino-Poster.jpg"},
                            new Movie(){MovieTitle="Scarace",Rating=2,ImageUrl="Scarface.jpg",AgeLimit="18"},
                            new Movie(){MovieTitle="The Shining",Rating=5,ImageUrl="~/Content/Images/Shining.jpg",AgeLimit="18"},
                            new Movie(){MovieTitle="One Flew Over the Cuckoo's Nest",Rating=4,ImageUrl="~/Content/Images/cuckoo.jpg",AgeLimit="18"},
                            new Movie(){MovieTitle="Casino",Rating=5,ImageUrl="~/Content/Images/Casino_poster.jpg" },
                            new Movie(){MovieTitle="Goodfellas",Rating=2,ImageUrl="~/Content/Images/goodfellas_poster.jpg"},
                            new Movie(){MovieTitle="Deerhunter",Rating=5,ImageUrl="~/Content/Images/The_Deer_Hunter_poster.jpg"},
                            new Movie(){MovieTitle="Raging Bull",Rating=3,ImageUrl="~/Content/Images/raging_bull.jpg"},
                            new Movie(){MovieTitle="Deerhunter",Rating=5,ImageUrl="~/Content/Images/The_Deer_Hunter_poster.jpg"},
                            new Movie(){MovieTitle="Carlito's Way",Rating=3,ImageUrl="~/Content/Images/Carlito's_Way.jpg"},
                            new Movie(){MovieTitle="Gladiator",Rating=5,ImageUrl="~/Content/Images/gladiator_ver3_xlg.jpg",AgeLimit="18"},
                            new Movie(){MovieTitle="Departed",Rating=3,ImageUrl="~/Content/Images/departrd.jpg",AgeLimit="18"}

            };
     

                  var actors = new List<Actor>()
                  {
                  new Actor()
                  {
                      ActorName="Marlon Brando",AwardsWon=8,DateOfBirth=DateTime.Parse("3/4/1924"),ImageUrl="http://static3.businessinsider.com/image/52ebed986da811b22d22d1ee-480/newsweek-cover-marlon-brando-godfather.jpg",
                      Movies= new List<Movie>(){movies[0],movies[1]},
                  },
                   new Actor()
                  {
                      ActorName="Al Pacino",AwardsWon=14,DateOfBirth=DateTime.Parse("25/4/1940"),ImageUrl="http://mkalty.org/wp-content/uploads/2014/07/936full-al-pacino.jpg",
                      Movies=new List<Movie>(){movies[2],movies[3]}
                  },
                    new Actor()
                  {
                      ActorName="Jack Nicholson",AwardsWon=10,DateOfBirth=DateTime.Parse("22/4/1937"),ImageUrl="http://upload.wikimedia.org/wikipedia/en/thumb/b/bb/The_shining_heres_johnny.jpg/170px-The_shining_heres_johnny.jpg",
                      Movies=new List<Movie>(){movies[4],movies[5]}
                  },
                    new Actor()
                  {
                      ActorName="Joe Pesci",AwardsWon=7,DateOfBirth=DateTime.Parse("22/9/1943"),ImageUrl="http://famousdude.com/images/joe-pesci-09.jpg",
                      Movies=new List<Movie>(){movies[6],movies[7]}
                  },
                    new Actor()
                  {
                      ActorName="Robert De Niro",AwardsWon=9,DateOfBirth=DateTime.Parse("17/8/1943"),ImageUrl="http://upload.wikimedia.org/wikipedia/commons/c/c0/Robert_De_Niro_KVIFF_portrait.jpg",
                      Movies=new List<Movie>(){movies[8],movies[9]}
                  },
                    new Actor()
                  {
                      ActorName="Sean Penn",AwardsWon=6,DateOfBirth=DateTime.Parse("17/8/1960"),ImageUrl="http://www.celebritypicnic.com/celebrities/sean-penn/sean-penn_8.jpg",
                      Movies=new List<Movie>(){movies[10],movies[11]}
                  },
                    new Actor()
                  {
                      ActorName="Russell Crowe",AwardsWon=8,DateOfBirth=DateTime.Parse("7/4/1964"),ImageUrl="http://mrkiii.com/wp-content/uploads/2013/06/still-of-russell-crowe-in-gladiatorul.jpg",
                      Movies=new List<Movie>(){movies[12],movies[13]}
                  },  
                  //new Actor()
                  //{
                  //    ActorName="Mick Lally",AwardsWon=50,DateOfBirth=DateTime.Parse("10/11/1945"),ImageUrl="http://cf.broadsheet.ie/wp-content/uploads/2010/08/20.jpg",
                  //    Movies=new List<Movie>(){movies[14],movies[15]}
                  //},  new Actor()
                  //{
                  //    ActorName="Brendan Gleeson",AwardsWon=14,DateOfBirth=DateTime.Parse("29/3/1955"),ImageUrl="http://pmcdeadline2.files.wordpress.com/2013/06/brendangleeson__130618001548.jpg",
                  //    Movies=new List<Movie>(){movies[16],movies[17]}
                  //}
                  
                

          
             

            };

                  var AM = new List<ActorMovie>() {  new ActorMovie(){MMovie=actors[0].Movies[0],AActor=actors[0],CharacterName="Vito Corleone"},
                                                     new ActorMovie(){MMovie=actors[0].Movies[1],AActor=actors[0],CharacterName="Ben Hur"},

                                                     new ActorMovie(){MMovie=actors[1].Movies[0],AActor=actors[1],CharacterName="Michael Corleone"},
                                                     new ActorMovie(){MMovie=actors[1].Movies[1],AActor=actors[1],CharacterName="Tony Montana"},

                                                     new ActorMovie(){MMovie=actors[2].Movies[0],AActor=actors[2],CharacterName="Randle Patrick McMurphy"},
                                                     new ActorMovie(){MMovie=actors[2].Movies[1],AActor=actors[2],CharacterName="Jack Torrance"},

                                                     new ActorMovie(){MMovie=actors[3].Movies[0],AActor=actors[3],CharacterName="Nicky Santoro"},
                                                     new ActorMovie(){MMovie=actors[3].Movies[1],AActor=actors[3],CharacterName="Tommy"},

                                                     new ActorMovie(){MMovie=actors[4].Movies[0],AActor=actors[4],CharacterName="Michael"},
                                                     new ActorMovie(){MMovie=actors[4].Movies[1],AActor=actors[4],CharacterName="Jake LaMotta"},

                                                     new ActorMovie(){MMovie=actors[5].Movies[0],AActor=actors[5],CharacterName="Nicky Santoro"},
                                                     new ActorMovie(){MMovie=actors[5].Movies[1],AActor=actors[5],CharacterName="Tommy"},

                                                     new ActorMovie(){MMovie=actors[6].Movies[0],AActor=actors[6],CharacterName="Nicky Santoro"},
                                                     new ActorMovie(){MMovie=actors[6].Movies[1],AActor=actors[6],CharacterName="Tommy"}
                                                 
                  
                  
                  };


            
            actors.ForEach(a => context.Actors.Add(a));
            AM.ForEach(a => context.ActorMovie.Add(a));
            

            context.SaveChanges();
        }
    }

    public class ActorMovieDB : DbContext
    {
        public DbSet<Actor> Actors { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<ActorMovie> ActorMovie { get; set; }
        public ActorMovieDB()
            : base("ActorMovieDB")
        {

        }
    }


    public class Actor
    {
        public int ActorId { get; set; }
        public string ActorName { get; set; }
        public virtual List<Movie> Movies { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string ImageUrl { get; set; }
        public int AwardsWon { get; set; }
        public virtual List<ActorMovie> ActorMovie { get; set; }


    }//End Class

    public class Movie
    {
        public int MovieId { get; set; }
        public string MovieTitle { get; set; }
        public virtual List<Actor> Actors { get; set; }
        [Range(1.5, int.MaxValue)]
        public int Rating { get; set; }
        public string ImageUrl { get; set; }
        public virtual List<ActorMovie> ActorMovie { get; set; }
        public string AgeLimit { get; set; }


    }//End Class

    public class ActorMovie
    {
        [Key, Column(Order = 0)]
        public int MovieId { get; set; }
        [Key, Column(Order = 1)]
        public int ActorId { get; set; }
        public string CharacterName { get; set; }

        public virtual Movie MMovie { get; set; }
        public virtual Actor AActor { get; set; }

    }


   
}