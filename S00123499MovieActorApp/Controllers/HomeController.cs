﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using S00123499MovieActorApp.Models;
using PagedList;
using PagedList.Mvc;

namespace S00123499MovieActorApp.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        public ActorMovieDB db = new ActorMovieDB();
        
        public ActionResult AutoComplete(string term)
        { 
            
            
                var model = db.Actors.
                    Where(a=>a.ActorName.StartsWith(term))
                    .Select(a=>new
                    {
                        label=a.ActorName
                    });
                return Json(model, JsonRequestBehavior.AllowGet);
                    
            
        }

        public ActionResult Index(string sortOrder,int page = 1)
        {

            using (var db = new ActorMovieDB())
            {
                var actors = db.Actors.Where(r=>sortOrder == null || r.ActorName.StartsWith(sortOrder));

                

                if (Request.IsAjaxRequest())
                {
                    return PartialView("_Actors", actors.ToList());
                }
                else
                {
                    return View(actors.OrderBy(r=>r.DateOfBirth).ToPagedList(page,4));
                }

            }
        }

        public PartialViewResult sortFilter(string sortOrder,int page = 1)
        {
            using (var db = new ActorMovieDB())
            {

         
            var actors = from a in db.Actors
                           select a;
            
            switch (sortOrder)
            {
                case "name_desc":
                    actors = actors.OrderByDescending(A => A.ActorName);
                    break;
                case "Date":
                    actors = actors.OrderBy(A => A.DateOfBirth);
                    break;
                case "awardsWon":
                    actors = actors.OrderByDescending(A => A.AwardsWon);
                    break;
                default:
                    actors = actors.OrderBy(A => A.ActorName);
                    break;
            }
            return PartialView("_Actors", actors.ToPagedList(page, 4));
            }
        }

        public ActionResult Details(int id)
        {
            using (var db = new ActorMovieDB())
            {
                var actor = db.Actors.Include("Movies").Include("ActorMovie").ToList().SingleOrDefault(i => i.ActorId == id);
                
                if (Request.IsAjaxRequest())
                {
                    return PartialView("_Details", actor);
                }
                
                return View(actor);
            }

        }

        

        [HttpGet]
        public PartialViewResult Create()
        {
           return PartialView("_Create");
        }

        [HttpPost]
        public ActionResult CreateActor(Actor A)
        {
            using (var db = new ActorMovieDB())
            {
                if (ModelState.IsValid)
                {
                    db.Actors.Add(A);
                    db.SaveChanges();
                    RedirectToAction("index");
                 }
                else
                {
                    RedirectToAction("index");

                }

                return View();
            }
        }
        [HttpGet]
        public PartialViewResult Delete(int id)
        {
            using (var db = new ActorMovieDB())
            {

                return PartialView("_Delete",db.Actors.Find(id));
            }
        }
        [HttpPost]
        public PartialViewResult DeleteActor(int id,int page =1)
        {
            
            using (var db = new ActorMovieDB())
            {
                var actors = from a in db.Actors select a;
                db.Actors.Remove(db.Actors.Find(id));
                db.SaveChanges();
                return PartialView("_Actors", actors.OrderBy(a => a.ActorName.ToPagedList(page, 4)));
            }
        }

        [HttpGet]
        public PartialViewResult CreateMovie()
        {
            return PartialView("_CreateMovie");
        }

        public ActionResult example()
        {
            return View();
        }
       
      
    }
}
